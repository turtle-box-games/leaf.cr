require "./spec_helper"

# Verify that every node type reports its type correct.
# Iterate over the NodeType enum to get all the node types.
{% for type_name in Leaf::NodeType.constants %}
Spectator.describe Leaf::{{type_name.id}}Node do
  describe "#node_type" do
    it "is {{type_name.id}}" do
      expect(new_{{type_name.downcase}}_node.node_type).to eq(Leaf::NodeType::{{type_name.id}})
    end
  end
end
{% end %}
