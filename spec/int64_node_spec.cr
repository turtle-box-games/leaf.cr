require "./spec_helper"

Spectator.describe Leaf::Int64Node do
  def integers
    [0_i64, -1_i64, 1_i64, 2_123_456_000_i64, -987_654_000_i64, Int64::MIN, Int64::MAX]
  end

  sample integers do |integer|
    let(node) { new_int64_node(integer) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(integer)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = integer.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
