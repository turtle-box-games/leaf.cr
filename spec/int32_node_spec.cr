require "./spec_helper"

Spectator.describe Leaf::Int32Node do
  def integers
    [0_i32, -1_i32, 1_i32, 2_123_456_i32, -987_654_i32, Int32::MIN, Int32::MAX]
  end

  sample integers do |integer|
    subject(node) { new_int32_node(integer) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(integer)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = integer.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
