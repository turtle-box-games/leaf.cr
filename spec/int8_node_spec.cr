require "./spec_helper"

Spectator.describe Leaf::Int8Node do
  def integers
    [0_i8, -1_i8, 1_i8, 50_i8, -75_i8, Int8::MIN, Int8::MAX]
  end

  sample integers do |integer|
    subject(node) { new_int8_node(integer) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(integer)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = integer.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
