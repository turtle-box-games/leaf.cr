require "./spec_helper"

Spectator.describe Leaf::BlobNode do
  # The quick brown fox jumps over the lazy dog
  QUICK_BROWN_FOX = Bytes[
    0x54, 0x68, 0x65, 0x20, 0x71, 0x75, 0x69, 0x63,
    0x6b, 0x20, 0x62, 0x72, 0x6f, 0x77, 0x6e, 0x20,
    0x66, 0x6f, 0x78, 0x20, 0x6a, 0x75, 0x6d, 0x70,
    0x73, 0x20, 0x6f, 0x76, 0x65, 0x72, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x6c, 0x61, 0x7a, 0x79, 0x20,
    0x64, 0x6f, 0x67]

  QUICK_BROWN_FOX_SHA1_HEXSTRING = "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"

  let(bytes) { Random.new.random_bytes }
  subject(node) { new_blob_node(bytes) }

  describe "#bytes" do
    subject { node.bytes }

    it "returns the value passed to #initialize" do
      is_expected.to eq(bytes)
    end
  end

  describe "#sha1" do
    subject { node.sha1 }

    it "returns a 20-byte array" do
      # 20 bytes is the length of a SHA-1 hash.
      expect(subject.size).to eq(20)
    end

    it "produces the correct value" do
      node = new_blob_node(QUICK_BROWN_FOX)
      expect(node.sha1.to_slice.hexstring).to eq(QUICK_BROWN_FOX_SHA1_HEXSTRING)
    end
  end

  describe "#inspect" do
    subject { node.inspect }

    it "reports the correct length" do
      is_expected.to match(/Length: #{bytes.size}/)
    end

    it "includes a SHA-1 hash" do
      is_expected.to match(/SHA-1: #{node.sha1.to_slice.hexstring}/)
    end

    context "when the size is less than 16 bytes" do
      let(bytes) { Random.new.random_bytes(8) }

      it "shows a hex string of the bytes" do
        is_expected.to match(/#{bytes.hexstring}/)
      end
    end

    context "when the size is greater than 16 bytes" do
      let(bytes) { Random.new.random_bytes(24) }

      it "truncates the hex string" do
        truncated = bytes.hexstring[0...32] + Leaf::BlobNode::TRUNCATE_CHAR
        regex = Regex.new(truncated) # Don't need to escape because it's hexadecimal.
        is_expected.to match(regex)
      end
    end
  end
end
