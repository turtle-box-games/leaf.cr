require "./spec_helper"

Spectator.describe Leaf::Float64Node do
  def floats
    [0_f64, 1_f64, -1_f64, 777_f64, -1024_f64, 0.123_f64, -0.567_f64,
     Float64::INFINITY, Float64::MIN, Float64::MAX]
  end

  sample floats do |float|
    subject(node) { new_float64_node(float) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(float)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = float.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end

  it "is NaN when passed to #initialize" do
    node = new_float64_node(Float64::NAN)
    expect(node.value).to be_nan
  end
end
