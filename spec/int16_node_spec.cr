require "./spec_helper"

Spectator.describe Leaf::Int16Node do
  def integers
    [0_i16, -1_i16, 1_i16, 2500_i16, -3333_i16, Int16::MIN, Int16::MAX]
  end

  sample integers do |integer|
    subject(node) { new_int16_node(integer) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(integer)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = integer.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
