require "./spec_helper"

Spectator.describe Leaf::FlagNode do
  def bools
    [true, false]
  end

  sample bools do |bool|
    subject(node) { new_flag_node(bool) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(bool)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = bool.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
