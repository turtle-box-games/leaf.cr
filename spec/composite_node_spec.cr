require "./spec_helper"

SAMPLE_COMPOSITE_ELEMENTS = {
  {"foo", Leaf::Int32Node.new},
  {"bar", Leaf::TimeNode.new},
  {"baz", Leaf::FlagNode.new},
}

def sample_composite_elements_hash
  Hash(String, Leaf::Node).new.tap do |hash|
    SAMPLE_COMPOSITE_ELEMENTS.each do |k, v|
      hash[k] = v
    end
  end
end

Spectator.describe Leaf::CompositeNode do
  it "has the expected nodes" do
    hash = sample_composite_elements_hash
    expect(new_composite_node(hash).to_a).to eq(sample_composite_elements_hash.to_a)
  end

  describe "#initialize" do
    context "parameterless initializer" do
      it "creates an empty node" do
        expect(new_composite_node.size).to eq(0)
      end
    end

    context "enumerable initializer" do
      it "populates the node" do
        expect(new_composite_node(SAMPLE_COMPOSITE_ELEMENTS).to_a).to eq(sample_composite_elements_hash.to_a)
      end
    end
  end

  describe "#each" do
    context "with a block" do
    end

    context "without a block" do
      it "returns an iterator" do
        expect(new_composite_node.each).to be_a(Iterator(Tuple(String, Leaf::Node)))
      end

      it "returns a correct iterator" do
        iterator = new_composite_node(SAMPLE_COMPOSITE_ELEMENTS).each
        expect(iterator.size).to eq(SAMPLE_COMPOSITE_ELEMENTS.size)
      end
    end
  end

  describe "#each_key" do
    context "with a block" do
    end

    context "without a block" do
    end
  end

  describe "#each_node" do
    context "with a block" do
    end

    context "without a block" do
    end
  end

  describe "#[]" do
  end

  describe "#[]?" do
  end

  describe "#has_key?" do
  end

  describe "#has_node?" do
  end

  describe "#fetch" do
    context "with no default" do
    end

    context "with a default" do
    end

    context "with a block" do
    end
  end

  describe "#empty?" do
    context "when empty" do
      it "returns true" do
        expect(new_composite_node.empty?).to be_true
      end
    end

    context "when not empty" do
      it "returns false" do
        expect(new_composite_node(SAMPLE_COMPOSITE_ELEMENTS).empty?).to be_false
      end
    end
  end

  describe "#keys" do
  end

  describe "#nodes" do
  end

  describe "#size" do
    it "is the number of nodes" do
      elements = sample_composite_elements_hash
      node = new_composite_node(elements)
      expect(node.size).to eq(elements.size)
    end
  end

  describe "#inspect" do
    it "reports the length" do
      elements = sample_composite_elements_hash
      node = new_composite_node(elements)
      expect(node.inspect).to match(/#{elements.size} items/)
    end
  end
end
