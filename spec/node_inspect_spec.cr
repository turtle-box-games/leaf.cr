require "./spec_helper"

# Verify that every node type reports its type correctly in `#inspect`.
# Iterate over the NodeType enum to get all the node types.
{% for type_name in Leaf::NodeType.constants %}
Spectator.describe Leaf::{{type_name.id}}Node do
  describe "#inspect" do
    it "has the form {{type_name.id}}[...]" do
      expect(new_{{type_name.downcase}}_node.inspect).to match(/^{{type_name.id}}\[.*?\]$/)
    end
  end
end
{% end %}
