require "./spec_helper"

Spectator.describe Leaf::TimeNode do
  describe "#value" do
    it "returns the value passed to .new" do
      value = Time.unix(1533666538) # Seconds since the epoch when this test was written.
      node = new_time_node(value)
      expect(node.value).to eq(value)
    end

    it "works for values before the Unix epoch" do
      value = Time.utc(1900, 1, 1, 0, 0, 0)
      node = new_time_node(value)
      expect(node.value).to eq(value)
    end
  end

  describe "#inspect" do
    it "contains the stringified value" do
      node = new_time_node
      string = node.value.to_s # We're not testing precision here.
      regex = Regex.new(Regex.escape(string))
      expect(node.inspect).to match(regex)
    end
  end
end
