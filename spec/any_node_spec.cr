require "./spec_helper"

Spectator.describe Leaf::AnyNode do
  # Test the AnyNode class with all node types.
  {% for type_name in Leaf::NodeType.constants %}
    context "{{type_name.id}}Node" do
      let(node) { new_{{type_name.downcase}}_node }
      subject(any) { described_class.new(node) }

      describe "#node_type" do
        subject { any.node_type }

        it "returns {{type_name.id}}" do
          is_expected.to eq(Leaf::NodeType::{{type_name.id}})
        end
      end

      describe "#inspect" do
        subject { any.inspect }

        it "is the same as the original node's" do
          is_expected.to eq(node.inspect)
        end
      end

      describe "#==" do
        context "against wrapped node" do
          it "is true" do
            expect { any == node }.to be_true
          end
        end

        context "different node type" do
          it "is false" do
            other = {% if type_name == "Flag" %}
              new_float64_node
            {% else %}
              new_flag_node
            {% end %}
            expect { any == other }.to be_false
          end
        end

        {% if type_name == "List" %}
          context "element of different type" do
            it "is false" do
              node = new_list_node(Leaf::Float64Node)
              other = new_list_node(Leaf::StringNode)
              any = Leaf::AnyNode.new(node)
              expect { any == other}.to be_false
            end
          end
        {% end %}
      end

      # Check all combinations of `#as_TYPE` and `#as_TYPE?`.
      {% for other_type_name in Leaf::NodeType.constants %}
        # List nodes are handled differently.
        {% unless other_type_name == "List" %}
          describe "#as_{{other_type_name.downcase}}" do
            subject { any.as_{{other_type_name.downcase}} }

            # When the node types match,
            # `#as_TYPE` should return the wrapped node as the correct type.
            {% if type_name == other_type_name %}
              it "returns a casted node" do
                is_expected.to eq(node)
              end
            # When the nodes types don't match,
            # `#as_TYPE` should raise an error.
            {% else %}
              it "raises an error" do
                expect { subject }.to raise_error(TypeCastError)
              end
            {% end %}
          end

          describe "#as_{{other_type_name.downcase}}?" do
            subject { any.as_{{other_type_name.downcase}}? }

            # When the node types match,
            # `#as_TYPE?` should return the wrapped node as the correct type.
            {% if type_name == other_type_name %}
              it "returns a casted node" do
                is_expected.to eq(node)
              end
            # When the nodes types don't match,
            # `#as_TYPE?` should return `nil`.
            {% else %}
              it "returns nil" do
                is_expected.to be_nil
              end
            {% end %}
          end
        {% end %}
      {% end %}

      describe "#as_list" do
        subject { any.as_list(Leaf::Int32Node) }

        # When the node types match,
        # `#as_list` should return the wrapped node as the correct type.
        {% if type_name == "List" %}
          context "with correct element type" do
            it "returns a casted node" do
              is_expected.to eq(node)
            end
          end

          context "with different element type" do
            it "raises an error" do
              expect { any.as_list(Leaf::StringNode) }.to raise_error(TypeCastError)
            end
          end
        # When the nodes types don't match,
        # `#as_list` should raise an error.
        {% else %}
          it "raises an error" do
            expect { subject }.to raise_error(TypeCastError)
          end
        {% end %}
      end

      describe "#as_list?" do
        subject { any.as_list?(Leaf::Int32Node) }

        # When the node types match,
        # `#as_list?` should return the wrapped node as the correct type.
        {% if type_name == "List" %}
          context "with correct element type" do
            it "returns a casted node" do
              is_expected.to eq(node)
            end
          end

          context "with different element type" do
            it "returns nil" do
              expect(any.as_list?(Leaf::StringNode)).to be_nil
            end
          end
        # When the nodes types don't match,
        # `#as_list?` should return `nil`.
        {% else %}
          it "returns nil" do
            is_expected.to be_nil
          end
        {% end %}
      end
    end
  {% end %}
end
