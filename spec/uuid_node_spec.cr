require "./spec_helper"

Spectator.describe Leaf::UUIDNode do
  def uuid_strings
    [
      "00000000-0000-0000-0000-000000000000", # Empty
      "7a01e2d3-da0c-1153-8795-2165153678dc", # V1
      "2a00053e-18de-2f9e-a5e2-2f2dfc7e1cb3", # V2
      "9472098f-a010-384f-92eb-d5108f82d862", # V3
      "d6d19e9c-09c8-4d40-bbbe-6968be614ead", # V4
      "09038597-d82e-5dd4-91a0-03ee994d4416", # V5
    ]
  end

  sample uuid_strings do |string|
    let(uuid) { UUID.new(string) }
    subject(node) { new_uuid_node(uuid) }

    describe "#value" do
      subject { node.value }

      it "returns the passed to .new" do
        is_expected.to eq(uuid)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end
end
