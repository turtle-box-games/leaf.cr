require "./spec_helper"

Spectator.describe Leaf::StringNode do
  LONG_STRING = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " * 100

  def strings
    ["", " ", "\t", "\n", "\"",
     "`~!@#$%^&*()-=_+,.<>/?;:'\"",
     "foobar", "foo\nbar\nbaz", ""]
  end

  sample strings do |string|
    subject(node) { new_string_node(string) }

    describe "#value" do
      subject { node.value }

      it "returns the value set by #initialize" do
        is_expected.to eq(string)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the expected string" do
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end

      it "reports the length" do
        is_expected.to match(/Length: #{string.size}/)
      end
    end
  end

  context "with a long string" do
    subject(node) { new_string_node(LONG_STRING) }

    describe "#value" do
      subject { node.value }

      it "handles long strings" do
        expect(new_string_node(LONG_STRING).value).to eq(LONG_STRING)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "truncates long (> 50 chars) strings" do
        truncated = LONG_STRING[0...50] + Leaf::StringNode::TRUNCATE_CHAR
        regex = Regex.new(Regex.escape(truncated))
        is_expected.to match(regex)
      end

      it "reports the correct length for long (> 50 chars) strings" do
        is_expected.to match(/Length: #{LONG_STRING.size}/)
      end
    end
  end
end
