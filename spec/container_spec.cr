require "./spec_helper"

Spectator.describe Leaf::Container do
  describe "#root" do
    it "is the expected node" do
      node = new_int32_node
      container = Leaf::Container.new(node)
      expect(container.root).to eq(node)
    end

    abstract struct Base
      def ==(other : Int32)
        true
      end
    end

    struct Derived < Base
    end
  end
end
