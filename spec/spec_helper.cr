require "spectator"
require "../src/leaf"

# Create utility methods for each node type
# that build a construct a node of that type.
# Iterate over the NodeType enum to get all the node types.
{% for type_name in Leaf::NodeType.constants %}
  def new_{{type_name.downcase}}_node(*args)
    {% if type_name == "List" %}
      Leaf::ListNode(Leaf::Int32Node).new(*args)
    {% else %}
      Leaf::{{type_name.id}}Node.new(*args)
    {% end %}
  end
{% end %}

def new_list_node(element_type : T.class, *args) forall T
  Leaf::ListNode(T).new(*args)
end

# Create utility methods for creating a list of nodes.
{% for type_name in Leaf::NodeType.constants %}
  def new_{{type_name.downcase}}_nodes(count, *args)
    {% if type_name == "List" %}
      Array(Leaf::ListNode(Leaf::Int32Node)).new(count) do
        Leaf::ListNode(Leaf::Int32Node).new(*args)
      end
    {% else %}
      Array(Leaf::{{type_name.id}}Node).new(count) do
        Leaf::{{type_name.id}}Node.new(*args)
      end
    {% end %}
  end
{% end %}
