require "./spec_helper"

Spectator.describe Leaf::Float32Node do
  def floats
    [0_f32, 1_f32, -1_f32, 777_f32, -1024_f32, 0.123_f32, -0.567_f32,
     Float32::INFINITY, Float32::MIN, Float32::MAX]
  end

  sample floats do |float|
    subject(node) { new_float32_node(float) }

    describe "#value" do
      subject { node.value }

      it "returns the value passed to #initialize" do
        is_expected.to eq(float)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "contains the stringified value" do
        string = float.inspect
        regex = Regex.new(Regex.escape(string))
        is_expected.to match(regex)
      end
    end
  end

  it "is NaN when passed to #initialize" do
    node = new_float32_node(Float32::NAN)
    expect(node.value).to be_nan
  end
end
