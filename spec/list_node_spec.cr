require "./spec_helper"

Spectator.describe Leaf::ListNode do
  {% for element_type in Leaf::NodeType.constants %}
    context "{{element_type.id}} element type" do
      let(elements) { new_{{element_type.downcase}}_nodes(5) }
      subject(node) { new_list_node(typeof(elements.first), elements) }

      it "contains the expected items" do
        expect(node.to_a).to eq(elements)
      end

      describe "#initialize" do
        context "with no arguments" do
          it "creates an empty list" do
            expect(new_list_node.size).to eq(0)
          end
        end
      end

      describe "#size" do
        subject { node.size }

        it "is the number of elements" do
          is_expected.to eq(5)
        end
      end

      describe "#unsafe_fetch" do
        subject { node.unsafe_fetch(index) }

        context "with first index (0)" do
          let(index) { 0 }

          it "returns the first item" do
            is_expected.to eq(elements[0])
          end
        end

        context "with a middle index (2)" do
          let(index) { 2 }

          it "returns the middle item" do
            is_expected.to eq(elements[2])
          end
        end

        context "with last index (4)" do
          let(index) { 4 }

          it "returns the last item" do
            is_expected.to eq(elements[4])
          end
        end
      end

      {% unless element_type == "List" %} # Handle lists of lists separately.
        context "with element type {{element_type.id}}" do
          describe "#element_type" do
            it "returns {{element_type.id}}" do
              expect(new_list_node(Leaf::{{element_type.id}}Node).element_type).to eq(Leaf::NodeType::{{element_type.id}})
            end
          end

          describe "#inspect" do
            subject { node.inspect }

            it "reports the length" do
              is_expected.to match(/5 items/)
            end

            it "reports the element type" do
              is_expected.to match(/Type: {{element_type.id}}/)
            end
          end
        end
      {% end %}
    end
  {% end %}

  context "with list of lists" do
    let(elements) { new_list_nodes(5) }
    subject(node) { new_list_node(typeof(elements.first), elements) }

    describe "#element_type" do
      it "returns List" do
        expect(new_list_node(Leaf::ListNode(Leaf::Int32Node)).element_type).to eq(Leaf::NodeType::List)
      end
    end

    describe "#inspect" do
      subject { node.inspect }

      it "reports the length" do
        is_expected.to match(/5 items/)
      end

      it "reports the element type" do
        is_expected.to match(/Type: List/)
      end
    end
  end
end
