require "./leaf/*"

# Serialization library.
module Leaf
  # Current version of the Leaf library.
  VERSION = "0.1.0"
end
