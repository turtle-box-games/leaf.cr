require "./any_node"
require "./list_node"
require "./node"
require "./node_type"

module Leaf
  # Collection of nodes, each with the same type.
  # Nodes are accessed by an integer index.
  # This collection can be thought of as an array.
  # The nodes in the collection can be any type
  # (including `ListNode` and `CompositeNode`)
  # as long as all nodes are the same type.
  #
  # Nodes in the list are untyped,
  # meaning they must be cast before making sense of them.
  struct UntypedListNode < Node
    include Indexable(AnyNode)

    # Type of the nodes in the list.
    getter element_type : NodeType

    @elements : Array(Node)

    # Creates an empty list node.
    def initialize(@element_type)
      @elements = [] of Node
    end

    # Creates a list node with an initial set of elements.
    # All nodes must be of the same type.
    # An `ArgumentError` is raised if they aren't.
    def initialize(@element_type, elements : Enumerable(T)) forall T
      if elements.all? { |element| element.node_type == @element_type }
        @elements = elements.to_a
      else
        raise ArgumentError.new("Elements must all be of type #{@element_type}")
      end
    end

    # Creates a list node with an initial set of elements.
    # The nodes are not checked if they are of the same specified type.
    private def initialize(@element_type, @elements)
    end

    # Creates a list node with an initial set of elements.
    # The nodes are not checked if they are of the same specified type.
    protected def self.unchecked_new(element_type, elements : Enumerable(Node))
      UntypedListNode.new(element_type, elements.to_a)
    end

    # Retrieves the value representing the node's type.
    # This will always be `NodeType::List`.
    def node_type : NodeType
      NodeType::List
    end

    # Retrieves the number of elements in the list.
    def size
      @elements.size
    end

    # Retrieves the node at the specified index without checking bounds.
    def unsafe_fetch(index : Int) : AnyNode
      node = @elements.unsafe_fetch(index)
      AnyNode.new(node)
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      inspect_field(io, "Type", element_type)
      io << @elements.size
      io << " items"
    end

    def_equals_and_hash @element_type, @elements

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end

    # Converts to a typed list.
    # Requires that the element type is known at compile-time.
    def to_list(element_type : T.class) : ListNode(T) forall T
      ListNode.new(@elements.map(&.as(T)))
    end
  end
end
