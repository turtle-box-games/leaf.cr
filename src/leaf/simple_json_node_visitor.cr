require "base64"
require "json"
require "uuid/json"
require "./node_visitor"

module Leaf
  # Writes node contents to a stream in JSON format.
  # This is a one-way serialization.
  # Node type information is not stored in the JSON, so it cannot be deserialized.
  # However, this produces a much cleaner and simple JSON structure.
  private class SimpleJSONNodeVisitor < NodeVisitor
    # Creates the visitor.
    # *builder* refers to the JSON builder used to produce JSON output.
    def initialize(@builder : JSON::Builder)
    end

    # Writes a flag node to the stream.
    def visit(node : FlagNode) : Nil
      node.value.to_json(@builder)
    end

    # Writes an 8-bit integer node to the stream.
    def visit(node : Int8Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a 16-bit integer node to the stream.
    def visit(node : Int16Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a 32-bit integer node to the stream.
    def visit(node : Int32Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a 64-bit integer node to the stream.
    def visit(node : Int64Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a 32-bit floating-point node to the stream.
    def visit(node : Float32Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a 64-bit floating-point node to the stream.
    def visit(node : Float64Node) : Nil
      node.value.to_json(@builder)
    end

    # Writes a string node to the stream.
    def visit(node : StringNode) : Nil
      node.value.to_json(@builder)
    end

    # Writes a time node to the stream.
    def visit(node : TimeNode) : Nil
      node.value.to_json(@builder)
    end

    # Writes a UUID node to the stream.
    def visit(node : UUIDNode) : Nil
      node.value.to_json(@builder)
    end

    # Writes a blob node to the stream.
    def visit(node : BlobNode) : Nil
      base64_string = Base64.strict_encode(node.bytes)
      @builder.string(base64_string)
    end

    # Writes a list node (and its children) to the stream.
    def visit(node : ListNode | UntypedListNode) : Nil
      @builder.array do
        node.each(&.accept(self))
      end
    end

    # Writes a composite node (and its children) to the stream.
    def visit(node : CompositeNode) : Nil
      @builder.object do
        node.each do |key, child|
          @builder.field(key) { child.accept(self) }
        end
      end
    end
  end
end
