require "./node_type"

module Leaf
  # Provides the minimal set of methods needed
  # to write data types to a binary serialization.
  # Is compatible with `BinaryReader`.
  private struct BinaryWriter
    # Node data is stored in little endian.
    alias ByteFormat = IO::ByteFormat::LittleEndian

    # Creates the writer.
    # Writes to the provided *io* stream.
    def initialize(@io : IO)
    end

    # Writes a boolean to the stream.
    def write(value : Bool) : Nil
      write(value ? 1_i8 : 0_i8)
    end

    # Writes an 8-bit integer to the stream.
    def write(value : Int8) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a 16-bit integer to the stream.
    def write(value : Int16) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a 32-bit integer to the stream.
    def write(value : Int32) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a 64-bit integer to the stream.
    def write(value : Int64) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a 32-bit floating-point number to the stream.
    def write(value : Float32) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a 64-bit floating-point number to the stream.
    def write(value : Float64) : Nil
      @io.write_bytes(value, ByteFormat)
    end

    # Writes a string to the stream.
    def write(value : String) : Nil
      # The string is prefixed with a 16-bit length.
      bytes = value.to_slice
      write(bytes.size.to_i16)
      write(bytes)
    end

    # Writes a node type to the stream.
    def write(type : NodeType) : Nil
      @io.write_bytes(type.to_u8, ByteFormat)
    end

    # Writes raw bytes to the stream.
    def write(bytes : Bytes) : Nil
      @io.write(bytes)
    end
  end
end
