require "./node"
require "openssl/sha1"

module Leaf
  # Storage for arbitrary data (array of bytes).
  struct BlobNode < Node
    # Retrieves the value representing the node's type.
    # This will always be `NodeType::Blob`.
    def node_type : NodeType
      NodeType::Blob
    end

    # Array of bytes in this blob.
    getter bytes : Bytes

    # Creates a new node with an initial value.
    def initialize(@bytes = Bytes.empty)
    end

    # Generates a SHA-1 hash representation of the bytes.
    # This can be used to easily validate the contents.
    def sha1
      size = LibC::SizeT.new(@bytes.size)
      OpenSSL::SHA1.hash(@bytes.to_unsafe, size)
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      length = @bytes.size
      inspect_field(io, "Length", length)
      inspect_field(io, "SHA-1", sha1.to_slice.hexstring)
      if length > TRUNCATE_LENGTH
        inspect_truncated_data(io)
      else
        io << @bytes.hexstring
      end
    end

    # Maximum number of bytes to display from `bytes` in `#to_s`.
    # The actual characters will be twice this,
    # because hexadecimal formatting is used.
    TRUNCATE_LENGTH = 16

    # Character used to indicate the bytes were truncated in `#to_s`.
    # This is a unicode ellipsis.
    TRUNCATE_CHAR = '\u{2026}'

    # Truncate the bytes for `#inspect`.
    private def inspect_truncated_data(io)
      subset = @bytes[0, TRUNCATE_LENGTH]
      io << subset.hexstring
      io << TRUNCATE_CHAR
    end

    def_equals_and_hash @bytes

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
