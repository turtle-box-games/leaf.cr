require "base64"
require "json"
require "uuid/json"
require "./node_visitor"

module Leaf
  # Writes node contents to a stream in JSON format.
  # This serializer includes node type information so that it can be deserialized.
  private class JSONNodeVisitor < NodeVisitor
    # Creates the visitor.
    # *builder* refers to the JSON builder used to produce JSON output.
    def initialize(@builder : JSON::Builder)
    end

    # Writes a flag node to the stream.
    def visit(node : FlagNode) : Nil
      scalar(node)
    end

    # Writes an 8-bit integer node to the stream.
    def visit(node : Int8Node) : Nil
      scalar(node)
    end

    # Writes a 16-bit integer node to the stream.
    def visit(node : Int16Node) : Nil
      scalar(node)
    end

    # Writes a 32-bit integer node to the stream.
    def visit(node : Int32Node) : Nil
      scalar(node)
    end

    # Writes a 64-bit integer node to the stream.
    def visit(node : Int64Node) : Nil
      scalar(node)
    end

    # Writes a 32-bit floating-point node to the stream.
    def visit(node : Float32Node) : Nil
      scalar(node)
    end

    # Writes a 64-bit floating-point node to the stream.
    def visit(node : Float64Node) : Nil
      scalar(node)
    end

    # Writes a string node to the stream.
    def visit(node : StringNode) : Nil
      scalar(node)
    end

    # Writes a time node to the stream.
    def visit(node : TimeNode) : Nil
      scalar(node)
    end

    # Writes a UUID node to the stream.
    def visit(node : UUIDNode) : Nil
      scalar(node)
    end

    # Writes a blob node to the stream.
    def visit(node : BlobNode) : Nil
      base64_string = Base64.strict_encode(node.bytes)
      @builder.object do
        @builder.field("type", node.node_type.to_s)
        @builder.field("size", node.bytes.size)
        @builder.field("sha1", node.sha1.to_slice.hexstring)
        @builder.field("bytes", base64_string)
      end
    end

    # Writes a list node (and its children) to the stream.
    def visit(node : ListNode | UntypedListNode) : Nil
      @builder.object do
        @builder.field("type", node.node_type)
        @builder.field("size", node.size)
        @builder.field("element_type", node.element_type)
        @builder.field("elements") do
          @builder.array do
            node.each(&.accept(self))
          end
        end
      end
    end

    # Writes a composite node (and its children) to the stream.
    def visit(node : CompositeNode) : Nil
      @builder.object do
        @builder.field("type", node.node_type.to_s)
        @builder.field("size", node.size)
        @builder.field("elements") do
          @builder.object do
            node.each do |key, child|
              @builder.field(key) { child.accept(self) }
            end
          end
        end
      end
    end

    # Produces a JSON object containing the node's type and value.
    private def scalar(node) : Nil
      @builder.object do
        @builder.field("type", node.node_type.to_s)
        @builder.field("value") { node.value.to_json(@builder) }
      end
    end
  end
end
