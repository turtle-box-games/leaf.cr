require "./binary_reader"
require "./node_type"

module Leaf
  # Constructs nodes by reading their contents from a binary serialized stream.
  private struct BinarySerializationNodeFactory
    # Creates the factory with the specified reader.
    def initialize(@reader : BinaryReader)
    end

    # Reads a node (and any children it might have) from the stream.
    # The node is constructed and returned.
    def build : Node
      type = @reader.read(NodeType)
      read(type)
    end

    # Gets a proc for the specified node type.
    # Calling the proc will read the specified type from the stream and return it.
    private def reader(type : NodeType) : -> Node
      case type
      when NodeType::Flag      then ->{ read(FlagNode).as(Node) }
      when NodeType::Int8      then ->{ read(Int8Node).as(Node) }
      when NodeType::Int16     then ->{ read(Int16Node).as(Node) }
      when NodeType::Int32     then ->{ read(Int32Node).as(Node) }
      when NodeType::Int64     then ->{ read(Int64Node).as(Node) }
      when NodeType::Float32   then ->{ read(Float32Node).as(Node) }
      when NodeType::Float64   then ->{ read(Float64Node).as(Node) }
      when NodeType::String    then ->{ read(StringNode).as(Node) }
      when NodeType::Time      then ->{ read(TimeNode).as(Node) }
      when NodeType::UUID      then ->{ read(UUIDNode).as(Node) }
      when NodeType::Blob      then ->{ read(BlobNode).as(Node) }
      when NodeType::List      then ->{ read(UntypedListNode).as(Node) }
      when NodeType::Composite then ->{ read(CompositeNode).as(Node) }
      else
        raise "Unrecognized node type #{type}"
      end
    end

    # Reads a specified node type from the stream.
    private def read(type : NodeType) : Node
      reader(type).call
    end

    # Reads a flag node node the stream.
    private def read(type : FlagNode.class) : FlagNode
      value = @reader.read(Bool)
      FlagNode.new(value)
    end

    # Reads an 8-bit integer node from the stream.
    private def read(type : Int8Node.class) : Int8Node
      value = @reader.read(Int8)
      Int8Node.new(value)
    end

    # Reads a 16-bit integer node from the stream.
    private def read(type : Int16Node.class) : Int16Node
      value = @reader.read(Int16)
      Int16Node.new(value)
    end

    # Reads a 32-bit integer node from the stream.
    private def read(type : Int32Node.class) : Int32Node
      value = @reader.read(Int32)
      Int32Node.new(value)
    end

    # Reads a 64-bit integer node from the stream.
    private def read(type : Int64Node.class) : Int64Node
      value = @reader.read(Int64)
      Int64Node.new(value)
    end

    # Reads a 32-bit floating-point node from the stream.
    private def read(type : Float32Node.class) : Float32Node
      value = @reader.read(Float32)
      Float32Node.new(value)
    end

    # Reads a 64-bit floating-point node from the stream.
    private def read(type : Float64Node.class) : Float64Node
      value = @reader.read(Float64)
      Float64Node.new(value)
    end

    # Reads a string node from the stream.
    private def read(type : StringNode.class) : StringNode
      value = @reader.read(String)
      StringNode.new(value)
    end

    # Reads a time node from the stream.
    private def read(type : TimeNode.class) : TimeNode
      seconds = @reader.read(Int64)
      nanoseconds = @reader.read(Int32)
      value = Time.utc(seconds: seconds, nanoseconds: nanoseconds)
      TimeNode.new(value)
    end

    # Reads a UUID node from the stream.
    private def read(type : UUIDNode.class) : UUIDNode
      bytes = @reader.read(16)
      value = UUID.new(bytes)
      UUIDNode.new(value)
    end

    # Reads a blob node from the stream.
    private def read(type : BlobNode.class) : BlobNode
      size = @reader.read(Int32)
      bytes = @reader.read(size)
      BlobNode.new(bytes)
    end

    # Reads a list node from the stream.
    private def read(type : UntypedListNode.class) : UntypedListNode
      size = @reader.read(Int32)
      element_type = @reader.read(NodeType)
      reader = reader(element_type)
      elements = size.times.map { reader.call }
      UntypedListNode.new(element_type, elements)
    end

    # Reads a composite node from the stream.
    private def read(type : CompositeNode.class) : CompositeNode
      size = @reader.read(Int32)
      elements = size.times.map do
        node_type = @reader.read(NodeType)
        key = @reader.read(String)
        node = read(node_type)
        {key, node}
      end
      CompositeNode.new(elements)
    end
  end
end
