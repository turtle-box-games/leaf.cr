module Leaf
  # Base class for all node visitors.
  private abstract class NodeVisitor
    {% for type_name in NodeType.constants %}
      # Operates on a {{type_name.id}} node.
      abstract def visit(node : {{type_name.id}}Node) : Nil
    {% end %}
  end
end
