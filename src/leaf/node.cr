require "./node_type"

module Leaf
  # Base class for all node types.
  abstract struct Node
    # Retrieves the value representing the node's type.
    abstract def node_type : NodeType

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected abstract def accept(visitor) : Nil

    # Creates a string representation of the node.
    # The string will be in the format: `TYPE[DATA]`.
    def inspect(io)
      io << node_type
      io << '['
      inspect_value(io)
      io << ']'
    end

    # All node types must implement this method.
    # The data written to `io` is what appears between the brackets
    # in the `#inspect` output.
    #
    # Example:
    #
    # ```
    # protected def inspect_inner(io)
    #   io << value
    # end
    # ```
    #
    # Would ultimately produce something like:
    # `Int32[1234567]`
    protected abstract def inspect_value(io)

    # Writes a field for `#inspect` in a standard format.
    # This can be used by subclasses when adding informational fields.
    private def inspect_field(io, field_name, value)
      io << field_name
      io << ": "
      io << value
      io << "; "
    end
  end
end
