module Leaf
  # Values that indicate the node's type when it is serialized.
  enum NodeType : UInt8
    Flag      = 0x01
    Int8      = 0x02
    Int16     = 0x03
    Int32     = 0x04
    Int64     = 0x05
    Float32   = 0x06
    Float64   = 0x07
    String    = 0x08
    Time      = 0x09
    UUID      = 0x0a
    Blob      = 0x0b
    List      = 0x0c
    Composite = 0x0d
  end
end
