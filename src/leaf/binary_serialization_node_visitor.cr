require "./binary_writer"
require "./node_visitor"

module Leaf
  # Writes node contents to a stream in a binary serialization format.
  private class BinarySerializationNodeVisitor < NodeVisitor
    # Creates the visitor.
    # *writer* refers to the binary writer used to write to the stream.
    def initialize(@writer : BinaryWriter)
    end

    # Writes a flag node to the stream.
    def visit(node : FlagNode) : Nil
      @writer.write(node.value)
    end

    # Writes an 8-bit integer node to the stream.
    def visit(node : Int8Node) : Nil
      @writer.write(node.value)
    end

    # Writes a 16-bit integer node to the stream.
    def visit(node : Int16Node) : Nil
      @writer.write(node.value)
    end

    # Writes a 32-bit integer node to the stream.
    def visit(node : Int32Node) : Nil
      @writer.write(node.value)
    end

    # Writes a 64-bit integer node to the stream.
    def visit(node : Int64Node) : Nil
      @writer.write(node.value)
    end

    # Writes a 32-bit floating-point node to the stream.
    def visit(node : Float32Node) : Nil
      @writer.write(node.value)
    end

    # Writes a 64-bit floating-point node to the stream.
    def visit(node : Float64Node) : Nil
      @writer.write(node.value)
    end

    # Writes a string node to the stream.
    def visit(node : StringNode) : Nil
      @writer.write(node.value)
    end

    # Number of seconds from January 1, 0001 to January 1, 1970 (Unix epoch).
    EPOCH_SECONDS = 62135596800_i64

    # Writes a time node to the stream.
    def visit(node : TimeNode) : Nil
      value = node.value.to_utc
      seconds = value.to_unix + EPOCH_SECONDS
      nanoseconds = value.nanosecond
      @writer.write(seconds)
      @writer.write(nanoseconds)
    end

    # Writes a UUID node to the stream.
    def visit(node : UUIDNode) : Nil
      bytes = node.value.bytes.to_slice
      @writer.write(bytes)
    end

    # Writes a blob node to the stream.
    def visit(node : BlobNode) : Nil
      bytes = node.bytes
      @writer.write(bytes.size)
      @writer.write(bytes)
    end

    # Writes a list node (and its children) to the stream.
    def visit(node : ListNode | UntypedListNode) : Nil
      @writer.write(node.size)
      @writer.write(node.element_type)
      node.each(&.accept(self))
    end

    # Writes a composite node (and its children) to the stream.
    def visit(node : CompositeNode) : Nil
      @writer.write(node.size)
      node.each do |key, child|
        @writer.write(child.node_type)
        @writer.write(key)
        child.accept(self)
      end
    end
  end
end
