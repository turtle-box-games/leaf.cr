require "./node"

module Leaf
  # Storage for a sequence of characters.
  # This type is suitable for short and medium length text.
  # The text is encoded with UTF-8.
  struct StringNode < Node
    # Retrieves the value representing the node's type.
    # This will always be `NodeType::String`.
    def node_type : NodeType
      NodeType::String
    end

    # Value of the node.
    getter value : String

    # Creates a new node with an initial value.
    def initialize(@value = "")
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Produces the string representation of the node's value.
    def to_s(io)
      io << @value
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      length = @value.size
      inspect_field(io, "Length", length)
      if length > TRUNCATE_LENGTH
        inspect_truncated_value(io)
      else
        io << @value
      end
    end

    # Maximum number of characters to display from `value` in `#to_s`.
    TRUNCATE_LENGTH = 50

    # Character used to indicate the text was truncated in `#to_s`.
    # This is a unicode ellipsis.
    TRUNCATE_CHAR = '\u{2026}'

    # Truncate the string given to `io` for `#to_s`.
    private def inspect_truncated_value(io)
      io << @value[0...TRUNCATE_LENGTH]
      io << TRUNCATE_CHAR
    end

    def_equals_and_hash @value

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
