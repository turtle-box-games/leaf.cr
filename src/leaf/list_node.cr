require "./node"

module Leaf
  # Collection of nodes, each with the same type.
  # Nodes are accessed by an integer index.
  # This collection can be thought of as an array.
  # The nodes in the collection can be any type
  # (including `ListNode` and `CompositeNode`)
  # as long as all nodes are the same type.
  struct ListNode(T) < Node
    include Indexable(T)

    @elements : Array(T)

    # Creates an empty list node.
    def initialize
      compile_time_type_check
      @elements = [] of T
    end

    # Creates a list node with an initial set of elements.
    def initialize(elements : Enumerable(T))
      compile_time_type_check
      @elements = elements.to_a
    end

    # Retrieves the value representing the node's type.
    # This will always be `NodeType::List`.
    def node_type : NodeType
      NodeType::List
    end

    # Type of the nodes in the list.
    def element_type : NodeType
      compile_time_type_check
      # This macro produces a `NodeType` enum value
      # that corresponds to the type specified by `T`.
      {% for type_name in NodeType.constants %} # Loop through all node types.
        # Check if the list's element type matches this iteration.
        {% if @type.type_vars.first.name.starts_with?("Leaf::#{type_name}Node") %}
          NodeType::{{type_name.id}} # Spit this out as the actual returned value.
        {% end %}
      {% end %}
    end

    # Retrieves the number of elements in the list.
    def size
      @elements.size
    end

    # Retrieves the node at the specified index without checking bounds.
    def unsafe_fetch(index : Int)
      @elements.unsafe_fetch(index)
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      inspect_field(io, "Type", element_type)
      io << @elements.size
      io << " items"
    end

    # Ensures that the generic type specified for elements in the list is a type of `Node`.
    # This runs at compile-time by using macros.
    private def compile_time_type_check
      # Check if any of the ancestors of the generic type vars are the base `Node` type.
      {% unless @type.type_vars.first.ancestors.any? { |t| t.name == "Leaf::Node" } %}
        {% raise "#{@type.name} must have a generic type variable \
          that inherits from Leaf::Node, not #{@type.type_vars.first.name}" %}
      {% end %}
    end

    def_equals_and_hash @elements

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
