require "./node"

module Leaf
  # Stores a Boolean value.
  struct FlagNode < Node
    # Retrieves the value representing the node's type.
    # This will always be `NodeType::Flag`.
    def node_type : NodeType
      NodeType::Flag
    end

    # Value of the node.
    getter value : Bool

    # Creates a new node with an initial value.
    def initialize(@value = false)
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Produces the string representation of the node's value.
    def to_s(io)
      io << @value
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      io << @value
    end

    def_equals_and_hash @value

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
