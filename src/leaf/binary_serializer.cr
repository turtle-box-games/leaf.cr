require "./binary_header"
require "./binary_reader"
require "./binary_serialization_node_factory"
require "./binary_serialization_node_visitor"
require "./binary_writer"
require "./container"

module Leaf
  # Reads and writes containers from and to streams in binary format.
  module BinarySerializer
    extend self

    # Writes a container to a stream.
    def write(container : Container, io) : Nil
      write_header(io)
      write_container(container, io)
    end

    # Reads a container from a stream.
    def read(io) : Container
      read_header(io)
      read_container(io)
    end

    # Writes the Leaf binary serialization format header to a stream.
    private def write_header(io)
      io.write_bytes(BinaryHeader.default, BinaryWriter::ByteFormat)
    end

    # Reads a Leaf binary serialization format header from a stream.
    # Raises an error if the header is invalid.
    private def read_header(io)
      io.read_bytes(BinaryHeader, BinaryReader::ByteFormat).tap do |header|
        raise "Invalid Leaf header" unless header.valid?
      end
    end

    # Writes the contents of a node container to a stream.
    private def write_container(container, io)
      root = container.root
      writer = BinaryWriter.new(io)
      writer.write(root.node_type)
      visitor = BinarySerializationNodeVisitor.new(writer)
      root.accept(visitor)
    end

    # Reads the contents of a node container from a stream.
    private def read_container(io)
      reader = BinaryReader.new(io)
      factory = BinarySerializationNodeFactory.new(reader)
      root = factory.build
      Container.new(root)
    end
  end
end
