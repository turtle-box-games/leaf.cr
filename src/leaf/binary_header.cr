module Leaf
  private struct BinaryHeader
    # Four-character code (FourCC) identifying the format.
    # Spells out "LEAF" in ASCII.
    SIGNATURE = StaticArray[76_u8, 69_u8, 65_u8, 70_u8]

    # Number of bytes in the signature.
    SIGNATURE_LENGTH = 4

    # Leaf binary format version.
    # Used for deserialization to verify supported versions.
    VERSION = 1

    # FourCC identifying the format.
    # Should match the `SIGNATURE` constant.
    getter signature : StaticArray(UInt8, SIGNATURE_LENGTH)

    # Leaf binary format version.
    getter version : Int32

    # Creates a header.
    def initialize(@signature, @version)
    end

    # Creates a default header.
    # This contains the latest supported version and a valid signature.
    def self.default
      BinaryHeader.new(SIGNATURE, VERSION)
    end

    # Reads a binary header from an stream.
    def self.from_io(io : IO, format : IO::ByteFormat = IO::ByteFormat::SystemEndian)
      signature = StaticArray(UInt8, SIGNATURE_LENGTH).new(0_u8)
      io.read_fully(signature.to_slice)
      version = io.read_bytes(Int32, format)
      BinaryHeader.new(signature, version)
    end

    # Writes the binary header to a stream.
    def to_io(io : IO, format : IO::ByteFormat = IO::ByteFormat::SystemEndian)
      io.write(@signature.to_slice)
      io.write_bytes(@version, format)
    end

    # Checks if the header is valid.
    def valid?
      @signature == SIGNATURE && @version == VERSION
    end
  end
end
