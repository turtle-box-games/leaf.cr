require "./node"

module Leaf
  # Wrapper for a node that has an unknown type at compile-time.
  # The underlying node's type can be discovered and extracted by using the `#as_` methods.
  struct AnyNode
    # Wrapped node.
    protected getter node : Node

    # Creates a new wrapper node.
    def initialize(@node : Node)
    end

    {% for type_name in NodeType.constants %}
      # Attempts to cast the node to a `{{type_name.id}}Node`.
      # If the node is not the same type, then an error is raised.
      def as_{{type_name.downcase}} : {{type_name.id}}Node
        @node.as({{type_name.id}}Node)
      end

      # Checks if the node is a `{{type_name.id}}Node`
      # and casts to it if the node is that type.
      # If the node isn't that type, then `nil` is returned.
      def as_{{type_name.downcase}}? : {{type_name.id}}Node?
        as_{{type_name.downcase}} if @node.is_a?({{type_name.id}}Node)
      end
    {% end %}

    # Attempts to cast the node to an `UntypedListNode`.
    # If the node is not a list node, then an error is raised.
    #
    # If the node is a `ListNode`,
    # then it will be attempted to cast it to an `UntypedListNode`.
    def as_list : UntypedListNode
      if (node = @node).is_a?(ListNode)
        UntypedListNode.new(node.element_type, node)
      else
        @node.as(UntypedListNode)
      end
    end

    # Checks if the node is an `UntypedListNode`
    # and casts to it if the node is that type.
    # Otherwise, `nil` is returned.
    #
    # If the node is a `ListNode`,
    # then it will be attempted to cast it to an `UntypedListNode`.
    def as_list? : UntypedListNode?
      if (node = @node).is_a?(ListNode)
        UntypedListNode.new(node.element_type, node)
      else
        @node.as?(UntypedListNode)
      end
    end

    # Attempts to cast the node to a `ListNode`.
    # The `element_type` must be the same as the list's element type.
    # If the node is not the same type, then an error is raised.
    #
    # If the node is an `UntypedListNode`,
    # then it will be attempted to cast it to a `ListNode`
    # with elements of type *element_type*.
    def as_list(element_type : T.class) forall T
      if (node = @node).is_a?(UntypedListNode)
        node.to_list(element_type)
      else
        @node.as(ListNode(T))
      end
    end

    # Checks if the node is a `ListNode`
    # and casts to it if the node is that type.
    # The `element_type` must be the same as the list's element type.
    # If the node isn't that type, then `nil` is returned.
    #
    # If the node is an `UntypedListNode`,
    # then it will be attempted to cast it to a `ListNode`
    # with elements of type *element_type*.
    def as_list?(element_type : T.class) forall T
      if (node = @node).is_a?(UntypedListNode)
        node.to_list(element_type)
      else
        @node.as?(ListNode(T))
      end
    end

    # Retrieves the type of the wrapped node.
    def node_type : NodeType
      @node.node_type
    end

    # Assumes the underlying node is a `ListNode` and returns the element
    # at the given *index*, or `nil` if out of bounds.
    # Raises if the underlying node is not a `ListNode`.
    def []?(index : Int) : self?
      case node = @node
      when ListNode, UntypedListNode
        node[index]?.try { |n| AnyNode.new(n) }
      else
        raise TypeCastError.new("Expected ListNode for #[]?(index : Int), not #{node.class}")
      end
    end

    # Assumes the underlying node is a `ListNode` and returns the element
    # at the given *index*.
    # Raises if the underlying node is not a `ListNode`.
    def [](index : Int) : self
      case node = @node
      when ListNode, UntypedListNode
        AnyNode.new(node[index])
      else
        raise TypeCastError.new("Expected ListNode for #[](index : Int), not #{node.class}")
      end
    end

    # Assumes the underlying node is a `CompositeNode` and returns the child node
    # for the given *key*, or `nil` if a node with that key doesn't exist.
    # Raises if the underlying node is not a `CompositeNode`.
    def []?(key : String | Symbol) : self?
      case node = @node
      when CompositeNode
        node[key]?.try { |n| AnyNode.new(n) }
      else
        raise TypeCastError.new("Expected CompositeNode for #[]?(key : String | Symbol), not #{node.class}")
      end
    end

    # Assumes the underlying node is a `CompositeNode` and returns the child node
    # for the given *key*.
    # Raises if the underlying node is not a `CompositeNode`.
    def [](key : String | Symbol) : self
      case node = @node
      when CompositeNode
        AnyNode.new(node[key])
      else
        raise TypeCastError.new("Expected CompositeNode for #[](key : String | Symbol), not #{node.class}")
      end
    end

    # Traverses the depth of a structure and returns the node.
    # Returns `nil` if not found.
    def dig?(key : String | Symbol | Int, *subkeys)
      if (node = self[key]?) && node.responds_to?(:dig?)
        node.dig?(*subkeys)
      end
    end

    # Retrieves a node with the specified *key*.
    # Effectively the same as `#[]?`
    def dig?(key : String | Int)
      self[key]?
    end

    # Traverses the depth of a structure and returns the node,
    # otherwise raises `KeyError`.
    def dig(key : String | Symbol | Int, *subkeys)
      if (node = self[key]) && node.responds_to?(:dig)
        return node.dig(*subkeys)
      end
      raise KeyError.new("Child node not diggable for key: #{key.inspect}")
    end

    # Retrieves a node with the specified *key*.
    # Effectively the same as `#[]`
    def dig(key : String | Symbol | Int)
      self[key]
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it the wrapped node.
    protected def accept(visitor)
      @node.accept(visitor)
    end

    # Writes the string representation of the node data to a stream.
    def to_s(io)
      @node.to_s(io)
    end

    # Writes a summary of the node data to a stream.
    def inspect(io)
      @node.inspect(io)
    end

    # Checks if another instance is equal to the wrapped node.
    def ==(other : self)
      @node == other.node
    end

    # Checks if another instance is equal to the wrapped node.
    def ==(other)
      @node == other
    end

    def_hash @node

    def dup
      AnyNode.new(@node.dup)
    end

    def clone
      AnyNode.new(@node.clone)
    end
  end
end
