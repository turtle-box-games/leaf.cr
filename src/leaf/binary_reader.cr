require "./node_type"

module Leaf
  # Provides the minimal set of methods needed
  # to read data types from a binary serialization.
  # Is compatible with `BinaryWriter`.
  private struct BinaryReader
    # Node data is stored in little endian.
    alias ByteFormat = IO::ByteFormat::LittleEndian

    # Creates the reader.
    # Reads from the provided *io* stream.
    def initialize(@io : IO)
    end

    # Reads a boolean from the stream.
    def read(type : Bool.class) : Bool
      !read(Int8).zero?
    end

    # Reads an 8-bit integer from the stream.
    def read(type : Int8.class) : Int8
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a 16-bit integer from the stream.
    def read(type : Int16.class) : Int16
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a 32-bit integer from the stream.
    def read(type : Int32.class) : Int32
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a 64-bit integer from the stream.
    def read(type : Int64.class) : Int64
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a 32-bit floating-point number from the stream.
    def read(type : Float32.class) : Float32
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a 64-bit floating-point number from the stream.
    def read(type : Float64.class) : Float64
      @io.read_bytes(type, ByteFormat)
    end

    # Reads a string from the stream.
    def read(type : String.class) : String
      # The string is prefixed with a 16-bit length.
      bytesize = read(Int16)
      @io.read_string(bytesize)
    end

    # Reads a node type from the stream.
    def read(type : NodeType.class) : NodeType
      value = @io.read_bytes(UInt8, ByteFormat)
      NodeType.from_value(value)
    end

    # Reads a pre-determined length of bytes from the stream.
    def read(size : Int) : Bytes
      Bytes.new(size).tap do |bytes|
        @io.read_fully(bytes)
      end
    end
  end
end
