require "json/builder"
require "./container"
require "./json_node_visitor"
require "./simple_json_node_visitor"

module Leaf
  # Reads and writes containers from and to streams in JSON.
  module JSONSerializer
    extend self

    # Writes a container to a stream.
    # This is a one-way serialization.
    # Node type information is not stored in the JSON, so it cannot be deserialized.
    # However, this produces a much cleaner and simple JSON structure.
    def write_simple(container : Container, io) : Nil
      builder = JSON::Builder.new(io)
      visitor = SimpleJSONNodeVisitor.new(builder)
      builder.document do
        container.root.accept(visitor)
      end
    end

    # Writes a container to a stream.
    # This format can be deserialized, since it contains node type information.
    def write(container : Container, io) : Nil
      builder = JSON::Builder.new(io)
      visitor = JSONNodeVisitor.new(builder)
      builder.document do
        builder.object do
          builder.field("version", 1)
          builder.field("root") { container.root.accept(visitor) }
        end
      end
    end
  end
end
