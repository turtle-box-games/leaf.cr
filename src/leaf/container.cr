require "./any_node"

module Leaf
  # Wraps one main node (which may have nested nodes) and any associated information.
  struct Container
    # Root node of the container.
    # All data in the node structure is contained within this node
    # and any nested nodes it may contain.
    getter root : Leaf::AnyNode

    # Creates a container with a root node.
    def initialize(root)
      @root = Leaf::AnyNode.new(root)
    end
  end
end
