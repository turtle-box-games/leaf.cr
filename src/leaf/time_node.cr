require "./node"

module Leaf
  # Storage for a date and time.
  # Smallest precision (unit) available is 1 nanosecond (10^-9).
  # The earliest time that can be stored is 0001-01-01 00:00:00.0 UTC.
  # The latest time that can be stored is 9999-12-31 23:59:59.999_999_999 UTC.
  struct TimeNode < Node
    # Retrieves the value representing the node's type.
    # This will always be `NodeType::Time`.
    def node_type : NodeType
      NodeType::Time
    end

    # Retrieves the value of the node.
    getter value : Time

    # Creates a new node with an initial value.
    def initialize(@value = Time.local)
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Produces the string representation of the node's value.
    def to_s(io)
      io << @value
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      io << @value
    end

    def_equals_and_hash @value

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
