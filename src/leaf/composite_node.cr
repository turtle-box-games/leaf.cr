require "./node"

module Leaf
  # Collection of nodes that can have different types.
  # Nodes are accessed by a string name (key).
  # This collection can be though of as a dictionary or hash map in programming.
  # The nodes in the collection can be any type (including `ListNode` and `CompositeNode`).
  struct CompositeNode < Node
    include Enumerable({String, Node})
    include Iterable({String, Node})

    @elements : Hash(String, Node)

    # Creates an empty composite node.
    def initialize
      @elements = {} of String => Node
    end

    # Creates a composite node with an initial set of elements.
    def initialize(elements : Enumerable({String, Node}))
      @elements = {} of String => Node
      elements.each do |k, v|
        @elements[k] = v
      end
    end

    # ditto
    def initialize(elements : Hash(String, Node))
      @elements = elements.dup
    end

    # Retrieves the value representing the node's type.
    # This will always be `NodeType::Composite`.
    def node_type : NodeType
      NodeType::Composite
    end

    # Enumerates through each child node.
    def each(&block : T -> _) : Nil
      @elements.each do |k, v|
        yield({k, v})
      end
    end

    # Returns an iterator over the composite entries.
    # Which behaves like an `Iterator` returning a `Tuple` consisting of the key and node types.
    def each
      @elements.each
    end

    # Calls the given block for each key-node pair and passes in the key.
    def each_key(&block : String -> _) : Nil
      @elements.each do |key, value|
        yield key
      end
    end

    # Returns an iterator over the composite keys.
    # Which behaves like an `Iterator` consisting of the key's types.
    def each_key
      @elements.each_key
    end

    # Calls the given block for each key-node pair and passes in the value.
    def each_node(&block : Node -> _) : Nil
      @elements.each do |key, value|
        yield value
      end
    end

    # Returns an iterator over the composite nodes.
    # Which behaves like an `Iterator` consisting of the node's types.
    def each_node
      @elements.each_value
    end

    # See also: `#fetch`
    def [](key : String | Symbol)
      fetch(key)
    end

    # Returns the node for the key given by *key*.
    # If not found, returns `nil`.
    def []?(key : String | Symbol)
      fetch(key, nil)
    end

    # Traverses the depth of a structure and returns the node.
    # Returns `nil` if not found.
    def dig?(key : String | Symbol, *subkeys)
      if (node = self[key]?) && node.responds_to?(:dig?)
        node.dig?(*subkeys)
      end
    end

    # Retrieves a node with the specified *key*.
    # Effectively the same as `#[]?`
    def dig?(key : String | Symbol)
      self[key]?
    end

    # Traverses the depth of a structure and returns the node,
    # otherwise raises `KeyError`.
    def dig(key : String | Symbol, *subkeys)
      if (node = self[key]) && node.responds_to?(:dig)
        return node.dig(*subkeys)
      end
      raise KeyError.new("Child node not diggable for key: #{key.inspect}")
    end

    # Retrieves a node with the specified *key*.
    # Effectively the same as `#[]`
    def dig(key : String | Symbol)
      self[key]
    end

    # Returns `true` when the key given by *key* exists, otherwise `false`.
    def has_key?(key : String | Symbol)
      @elements.has_key?(key.to_s)
    end

    # Returns `true` when the node given by *node* exists, otherwise `false`.
    def has_node?(node : Node)
      @elements.has_value?(node)
    end

    # Returns the node for the key given by *key*.
    # If not found, a `KeyError` is raised.
    def fetch(key : String | Symbol)
      @elements.fetch(key.to_s) { raise KeyError.new("Missing key: #{key.inspect}") }
    end

    # Returns the node for the key given by *key*,
    # or when not found, the value given by *default*.
    def fetch(key : String | Symbol, default)
      @elements.fetch(key.to_s) { default }
    end

    # Returns the node for the key given by *key*,
    # or when not found, calls the given block with the key.
    def fetch(key : String | Symbol)
      @elements.fetch(key.to_s) { yield key }
    end

    # Returns `true` when there are no sub-nodes.
    def empty?
      @elements.empty?
    end

    # Returns a new `Array` with all the keys.
    def keys
      @elements.keys
    end

    # Returns only the nodes as an `Array`.
    def nodes
      @elements.values
    end

    # Number of nodes in the collection.
    def size
      @elements.size
    end

    # Allows a node visitor to operate on the node.
    # Calls `#visit` on *visitor* passing it `self`.
    protected def accept(visitor) : Nil
      visitor.visit(self)
    end

    # Writes a summary of the node data to a stream.
    protected def inspect_value(io)
      io << @elements.size
      io << " items"
    end

    def_equals_and_hash @elements

    # Compares the current node to an unknown (wrapped) node.
    def ==(other : AnyNode)
      self == other.node
    end
  end
end
