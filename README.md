leaf.cr
=======

Serialization library

Installation
------------

Add this to your application's `shard.yml`:

```yaml
dependencies:
  leaf:
    gitlab: turtle-box-games/leaf.cr
```

Usage
-----

```crystal
require "leaf"
```

TODO: Write usage instructions here

Development
-----------

TODO: Write development instructions here

Contributing
------------

1. Fork it (<https://gitlab.com/turtle-box-games/leaf.cr/fork/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

Contributors
------------

- [arctic-fox](https://gitlab.com/arctic-fox) Michael Miller - creator, maintainer
